# Java Advanced Programming TA Code Repo

University of Isfahan (AP-982)

# Package `deitel8`: Chapter 8 Exercises

+ [`IntegerSet.java`](src/deitel8/IntegerSet.java) (8.13)
    + [`IntegerSet2.java`](src/deitel8/IntegerSet2.java): Extended `IntegerSet` to support values in range [`lowerBound`, `upperBound`]. Recommended for Data Structures course pre-studies.
+ [`Rational.java`](src/deitel8/Rational.java) (8.15)
+ [`HugeInteger.java`](src/deitel8/HugeInteger.java) (8.16): Positive integers only. Not implementing methods commented as optional won't receive any negative grading.