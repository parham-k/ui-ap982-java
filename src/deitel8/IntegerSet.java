package deitel8;

public class IntegerSet {

    private final boolean[] a = new boolean[100];

    public static IntegerSet union(IntegerSet s1, IntegerSet s2) {
        IntegerSet result = new IntegerSet();
        for (int i = 0; i < 100; i++)
            result.a[i] = s1.a[i] || s2.a[i];
        return result;
    }

    public static IntegerSet intersection(IntegerSet s1, IntegerSet s2) {
        IntegerSet result = new IntegerSet();
        for (int i = 0; i < 100; i++)
            result.a[i] = s1.a[i] && s2.a[i];
        return result;
    }

    public void addElement(int x) {
        a[x] = true;
    }

    public void deleteElement(int x) {
        a[x] = false;
    }

    @Override
    public String toString() {
        boolean isEmpty = true;
        for (boolean b : a)
            if (b)
                isEmpty = false;
        if (isEmpty)
            return "-----";
        else {
            String s = "";
            for (int i = 0; i < 100; i++)
                if (a[i])
                    s += i + " ";
            return s;
        }
    }

    public boolean isEqualTo(IntegerSet s) {
        for (int i = 0; i < 100; i++)
            if (a[i] != s.a[i])
                return false;
        return true;
    }

}
