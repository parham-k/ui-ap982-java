package deitel8;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Rational {

    private final int num, den;

    public Rational(int num, int den) {
        int gcd = gcd(num, den);
        this.num = num / gcd;
        this.den = den / gcd;
    }

    private static int gcd(int a, int b) {
        return a == 0 ? b : gcd(b % a, a);
    }

    public static Rational add(Rational r1, Rational r2) {
        return new Rational(r1.num * r2.den + r1.den * r2.num, r1.den * r2.den);
    }

    public static Rational subtract(Rational r1, Rational r2) {
        return new Rational(r1.num * r2.den - r1.den * r2.num, r1.den * r2.den);
    }

    public static Rational multiply(Rational r1, Rational r2) {
        return new Rational(r1.num * r2.num, r1.den * r2.den);
    }

    public static Rational divide(Rational r1, Rational r2) {
        return new Rational(r1.num * r2.den, r1.den * r2.num);
    }

    @Override
    public String toString() {
        return String.format("%s/%s", num, den);
    }

    public String toString(int places) {
        NumberFormat formatter = DecimalFormat.getInstance();
        formatter.setMaximumFractionDigits(places);
        return formatter.format((double) num / den);
    }

    public static void main(String[] args) {
        Rational r1 = new Rational(4, 6);
        Rational r2 = new Rational(3, 18);
        System.out.println(Rational.add(r1, r2));
        System.out.println(Rational.subtract(r1, r2));
        System.out.println(Rational.multiply(r1, r2));
        System.out.println(Rational.divide(r1, r2));
        System.out.println(r1);
        System.out.println(r2);
        System.out.println((new Rational(1, 3)).toString(4));
    }

}