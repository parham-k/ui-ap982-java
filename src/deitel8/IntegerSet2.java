package deitel8;

import java.util.ArrayList;
import java.util.Arrays;

public class IntegerSet2 {

    private final boolean[] contains;
    private final int lowerBound;

    public IntegerSet2(int lowerBound, int upperBound) {
        contains = new boolean[upperBound - lowerBound + 1];
        this.lowerBound = lowerBound;
    }

    public void addElement(int value) {
        contains[value - lowerBound] = true;
    }

    public void deleteElement(int value) {
        contains[value - lowerBound] = false;
    }

    public boolean contains(int value) {
        if (lowerBound <= value && value < lowerBound + contains.length)
            return contains[value - lowerBound];
        else
            return false;
    }

    public static IntegerSet2 union(IntegerSet2 set1, IntegerSet2 set2) {
        int lower = Math.min(set1.lowerBound, set2.lowerBound);
        int upper = Math.max(set1.lowerBound + set1.contains.length,
                set2.lowerBound + set2.contains.length);
        IntegerSet2 result = new IntegerSet2(lower, upper);
        for (int x = lower; x < upper; x++)
            if (set1.contains(x) || set2.contains(x))
                result.addElement(x);
        return result;
    }

    public static IntegerSet2 intersection(IntegerSet2 set1, IntegerSet2 set2) {
        int lower = Math.min(set1.lowerBound, set2.lowerBound);
        int upper = Math.max(set1.lowerBound + set1.contains.length,
                set2.lowerBound + set2.contains.length);
        IntegerSet2 result = new IntegerSet2(lower, upper);
        for (int x = lower; x < upper; x++)
            if (set1.contains(x) && set2.contains(x))
                result.addElement(x);
        return result;
    }

    public boolean isEmpty() {
        boolean isNotEmpty = false;
        for (boolean b : contains)
            isNotEmpty |= b;
        return !isNotEmpty;
    }

    public Integer[] getValues() {
        ArrayList<Integer> values = new ArrayList<>();
        for (int x = lowerBound; x < lowerBound + contains.length; x++)
            if (contains(x))
                values.add(x);
        return values.toArray(new Integer[0]);
    }

    @Override
    public String toString() {
        if (isEmpty())
            return "-----";
        return Arrays.toString(getValues());
    }

    public boolean isEqualTo(IntegerSet2 set) {
        int lower = Math.max(this.lowerBound, set.lowerBound);
        int upper = Math.min(this.lowerBound + this.contains.length,
                set.lowerBound + set.contains.length);
        for (int x = lower; x <= upper; x++)
            if (contains(x) != set.contains(x))
                return false;
        return true;
    }

    public static void main(String[] args) {
        IntegerSet2 set1 = new IntegerSet2(3, 8);
        IntegerSet2 set2 = new IntegerSet2(-5, 6);
        System.out.println(set1);
        set1.addElement(3);
        set1.addElement(5);
        set1.addElement(6);
        set1.addElement(8);
        System.out.println(set1);
        set2.addElement(-5);
        set2.addElement(3);
        set2.addElement(6);
        System.out.println(IntegerSet2.union(set1, set2));
        System.out.println(IntegerSet2.intersection(set1, set2));
        System.out.println(set1.isEqualTo(set2));
    }

}
