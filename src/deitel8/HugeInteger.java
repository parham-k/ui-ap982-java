package deitel8;

public class HugeInteger {

    private int[] digits = new int[40];

    public static HugeInteger parse(String value) {
        HugeInteger x = new HugeInteger();
        for (int i = 0; i < value.length(); i++)
            x.digits[i] = Character.getNumericValue(value.charAt(value.length() - 1 - i));
        return x;
    }

    @Override
    public String toString() {
        int lastIndex = 0;
        for (int i = 39; i >= 0 && lastIndex == 0; i--)
            if (digits[i] != 0) {
                lastIndex = i;
            }
        String s = "";
        for (int i = lastIndex; i >= 0; i--)
            s += digits[i];
        return s;
    }

    public HugeInteger add(HugeInteger x) {
        HugeInteger result = new HugeInteger();
        int carry = 0;
        for (int i = 0; i < 40; i++) {
            int s = this.digits[i] + x.digits[i] + carry;
            if (s >= 10) {
                carry = 1;
                s -= 10;
            } else
                carry = 0;
            result.digits[i] = s;
        }
        return result;
    }

    public HugeInteger subtract(HugeInteger x) {
        HugeInteger result = new HugeInteger();
        int borrow = 0;
        for (int i = 0; i < 40; i++) {
            int d = this.digits[i] - borrow - x.digits[i];
            if (d < 0) {
                borrow = 1;
                d += 10;
            } else
                borrow = 0;
            result.digits[i] = d;
        }
        return result;
    }

    public boolean isEqualTo(HugeInteger x) {
        for (int i = 0; i < 40; i++)
            if (this.digits[i] != x.digits[i])
                return false;
        return true;
    }

    public boolean isNotEqualTo(HugeInteger x) {
        return !isEqualTo(x);
    }

    public boolean isGreaterThan(HugeInteger x) {
        for (int i = 39; i >= 0; i--)
            if (this.digits[i] != x.digits[i])
                return this.digits[i] > x.digits[i];
        return true;
    }

    public boolean isGreaterThanOrEqualTo(HugeInteger x) {
        return isGreaterThan(x) || isEqualTo(x);
    }

    public boolean isLessThan(HugeInteger x) {
        return !isGreaterThanOrEqualTo(x);
    }

    public boolean isLessThanOrEqualTo(HugeInteger x) {
        return !isGreaterThan(x);
    }

    public boolean isZero() {
        for (int i = 0; i < 40; i++)
            if (this.digits[i] != 0)
                return false;
        return true;
    }

    // Optional
    public HugeInteger multiply(HugeInteger x) {
        HugeInteger result = new HugeInteger();
        for (int i = 0; i < x.digits.length; i++) {
            HugeInteger d = new HugeInteger();
            for (int j = 0; j < x.digits[i]; j++)
                d = d.add(this);
            int[] shifted = new int[40];
            for (int j = 0; j < 40 - i; j++)
                shifted[j + i] = d.digits[j];
            d.digits = shifted;
            result = result.add(d);
        }
        return result;
    }

    // Optional (inefficient solution)
    public HugeInteger divide(HugeInteger x) {
        HugeInteger result = new HugeInteger();
        HugeInteger thisCopy = HugeInteger.parse(this.toString());
        while (thisCopy.isGreaterThanOrEqualTo(x)) {
            result = result.add(HugeInteger.parse("1"));
            thisCopy = thisCopy.subtract(x);
        }
        return result;
    }

    // Optional
    public HugeInteger remainder(HugeInteger x) {
        return subtract(x.multiply(divide(x)));
    }

    public static void main(String[] args) {
        System.out.println(HugeInteger.parse("12345"));
        System.out.println(HugeInteger.parse("8").add(HugeInteger.parse("2")));
        System.out.println(HugeInteger.parse("7").subtract(HugeInteger.parse("3")));
        System.out.println(HugeInteger.parse("0").isZero());
        System.out.println(HugeInteger.parse("5").isGreaterThan(HugeInteger.parse("2")));
        System.out.println(HugeInteger.parse("5").isLessThanOrEqualTo(HugeInteger.parse("2")));
        System.out.println(HugeInteger.parse("321").multiply(HugeInteger.parse("123")));
        System.out.println(HugeInteger.parse("321").divide(HugeInteger.parse("123")));
        System.out.println(HugeInteger.parse("321").remainder(HugeInteger.parse("123")));
    }

}
